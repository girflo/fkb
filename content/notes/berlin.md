+++
title = "Berlin guide"
date = "2022-10-23"
[taxonomies]
tags = ["travel"]
[extra]
description = "personal recommendations in Berlin"
+++

{{ image(image_path="./images/berlin.jpg", alt="glitched picture of Berlin") }}

## Tips

- Always have a bit of cash on you, a lot of shops in Berlin don’t accept the card.
- Convenience stores are called Späti, they are everywhere and usually open late. It’s common for them to have some tables in front for people to enjoy a drink.
- Shops are closed on Sundays in Germany.

## Restaurants

### Vegan

- [1990 Vegans](https://www.openstreetmap.org/node/4682864191): Thai street food tapas, my favorite restaurant in Berlin, even if you aren't vegan I highly recommend you to try it.
- [Secret Garden](https://www.openstreetmap.org/node/6552674824): Vegan sushi, the restaurant itself is on top of a vegan supermarket and in front of a vegan shoe shop.

### Vegetarian & Vegan options

- [Curry 61](https://www.openstreetmap.org/node/2796753232): to try the famous [Currywurst](https://en.wikipedia.org/wiki/Currywurst), the sauce is excellent and they have a good vegan option.
- [Shakespeare and Sons](https://www.openstreetmap.org/node/6435179523): Great boiled bagels and also a nice English bookshop.
- [Goldies](https://www.openstreetmap.org/node/3216719426): Fries with topping, perfect comfort food.
- [Chungking noodles](https://www.openstreetmap.org/node/6978892023): Delicious Chongqing noodles with a lot of Sichuan pepper.
- [Wen cheng](https://www.openstreetmap.org/node/697751131): Biang Biang noodles, just go there it's so good.

## Museums

### Art & Exhibition

- [Hamburger Bahnhof](https://www.openstreetmap.org/way/175464203): Modern art museum (public).
- [KW Institute for Contemporary Art](https://www.openstreetmap.org/node/3641040703): Modern art museum (private).
- [Neurotitan gallery](https://www.openstreetmap.org/node/2741770297): Small exhibition room at the back of a book/art/zine store.
- [C/O Berlin](https://www.openstreetmap.org/node/2959465154): Exhibition (most of the time photography).
- [Sammlung Boros](https://www.openstreetmap.org/node/4219262248): A nazi bunker repurposed into a fruit storage place in the DDR then into a club after the fall of the wall then finally into modern art gallery. It's only guided tours where you learn about the art pieces and the history of the building.

### History

- [Stasi Museum](https://www.openstreetmap.org/node/281391655): Museum about the DDR's state security service. The building used to be the headquarters of the Stasi.
- [Dokumentationszentrum Berliner Mauer](https://www.openstreetmap.org/way/45664094): A free museum about Berlin's wall and in my opinion the best one, you can see a complete section of the wall with a watchtower as well as archive videos.

### Artifacts

- [Museum of Musical Instruments](https://www.openstreetmap.org/way/22990151): Small but cute museum showing a lot of musical instruments from all ages.

## Beers

In Berlin like everywhere in Germany there's a big choice of delicious beers, don't be shocked to see someone opening a beer in the s-bhan after leaving their workplace. To my knowledge, Berlin has two main kinds:

- Pilsner: Despite what everyone born in Berlin will say to you this kind of beer is bland, it's great for quenching your thirst but that's about it. The two main brands are Berliner Pilsner and Berliner Kindle. They taste the same yet everyone born in Berlin prefers one or the other (often depending in which part of Berlin they are from).
- [Berliner weisse](https://en.wikipedia.org/wiki/Berliner_Weisse): This one is a bit more interesting, it's a very old and sour beer. When you usually order it in a bar you will get it served with a sirup but it isn't so good. Recently sour beers are trendy again, which means that microbreweries produce great Berliner Weisse that you can enjoy without the sirup the ones from Brlo and Berliner Berg are great examples.

There's also Radlers which is a Pilsner mixed with citrus fruit soda, it's refreshing and low in alcohol so perfect for hot days.

You can of course also enjoy beers from other parts of Germany, the most common being:

- Weizenbier/Hefeweizen: Wheat beer served in high glasses, personally I like Franziskaner the best.
- Helles: Light lager, my recommendation if you don't know what to drink. Augustiner is my go-to beer.
- [Gose](https://en.wikipedia.org/wiki/Gose): Another old and sour beer that has its origin in Goslar.

## Beer shops

- [Drink Oase](https://www.openstreetmap.org/node/282406641): a späti with a large choice of beers.
- [Bierladen](https://www.openstreetmap.org/node/6041857820): same but in another kiez.
- [Biererei](https://www.openstreetmap.org/node/4034138661#map=19/52.50054/13.42239): Great place for fancy beers but fuck it's expensive, they also have a bar in the same street, also very expensive.

## Bars & Breweries

- [BRLO Brewhouse](https://www.openstreetmap.org/node/4367758889): Berliner microbrewery, their biergarten is the perfect sunny evenings hangout place. 
- [Hofbräu Berlin](https://www.openstreetmap.org/way/25003253): A big and ugly building opened by a Brewery from Munich, it's touristy but still interesting for the atmosphere, especially in the evening when it's full and people are dancing in front of the live folkloric band.
- [Holzmarkt](https://www.openstreetmap.org/way/763964269): A hangout place alongside the Spree.
- [Herman belgian bar](https://www.openstreetmap.org/node/2296975568): Good selection of Belgian beers.
- [Mikkeller](https://www.openstreetmap.org/node/5022231323): A Danish brewery offering lots of fancy beers.

## Events

- [Creative coding meetups](https://www.meetup.com/de-DE/creativecodeberlin/)
- [Lange nacht der museen](https://www.lange-nacht-der-museen.de)
- [Selbstgebautemusik festival](https://www.selbstgebautemusik.de/)

## Shops

- [Modulor](https://www.openstreetmap.org/node/1502983701): Huge art supply shop, there's a lot to see in there.

## Other

- [Botanical garden](https://www.openstreetmap.org/way/125194752): Probably the most beautiful place in Berlin, especially the greenhouses.
- [Il kino](https://www.openstreetmap.org/node/5153435222): Small independent cinema with a good selection of movies.
