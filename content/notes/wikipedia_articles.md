+++
title = "Wikipedia articles"
date = "2022-08-26"
[taxonomies]
tags = ["learning", "knowledge", "reading"]
[extra]
description = "keeping interesting wikipedia articles at hand"
+++

{{ small_image(image_path="./images/wiki.png", alt="glitched logo of wikipedia") }}

- [Aleatoric music](https://en.wikipedia.org/wiki/Aleatoric_music)
- [Astroid](https://en.wikipedia.org/wiki/Astroid)
- [Backpropagation](https://en.wikipedia.org/?title=Backpropagation)
- [Cistercian numerals](https://en.wikipedia.org/wiki/Cistercian_numerals)
- [Convolution](https://en.wikipedia.org/wiki/Convolution)
- [Halftone](https://en.wikipedia.org/wiki/Halftone)
- [Inverse kinematics](https://en.wikipedia.org/wiki/Inverse_kinematics)
- [Lambert's cosine law](https://en.wikipedia.org/wiki/Lambert%27s_cosine_law)
- [Penrose tiling](https://en.wikipedia.org/wiki/Penrose_tiling)
- [Physarum polycephalum](https://en.wikipedia.org/wiki/Physarum_polycephalum)
- [Player piano](https://en.wikipedia.org/wiki/Player_piano)
- [Ragnarök](https://en.wikipedia.org/wiki/Ragnar%C3%B6k)
- [Relative luminance](https://en.wikipedia.org/wiki/Relative_luminance)
- [Steganography](https://en.wikipedia.org/wiki/Steganography)
- [Wabi-sabi](https://en.wikipedia.org/wiki/Wabi-sabi)
