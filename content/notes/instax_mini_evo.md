+++
title = "Instax mini evo"
date = "2023-11-14"
[taxonomies]
tags = ["photography", "tuto"]
[extra]
description = "Pixel art tuto for using an Instax mini evo"
links = [["assets", "https://gitlab.com/girflo/instax-mini-evo"]]
+++

{{ image(image_path="./images/instax_mini_evo.gif", alt="animated tuto for using an instax mini evo") }}

## Tuto

The Instax mini evo is relatively easy to use compared to many cameras. Yet for folks that aren't use to take pictures with camera some of the features from this one could be overlooked. 

This small pixel art tuto was created for an event to show all the guest how to use this camera, I figured it woul not hurt to have it on this website.

{{ image_with_legend(image_path="./images/instax_mini_evo_step_1.png", alt="power on instax mini evo", legend="Step 1 - Power on") }}
{{image_with_legend(image_path="./images/instax_mini_evo_step_2.png", alt="choose filter on instax mini evo", legend="Step 2 - Choose filter") }}
{{ image_with_legend(image_path="./images/instax_mini_evo_step_3.png", alt="choose effect on instax mini evo", legend="Step 3 - Choose effect") }}
{{ image_with_legend(image_path="./images/instax_mini_evo_step_4.png", alt="take picture with an instax mini evo", legend="Step 4 - Snap it") }}
{{ image_with_legend(image_path="./images/instax_mini_evo_step_5.png", alt="print a picture on instax mini evo", legend="Step 5 - Print it") }}

## Sources

The pixel art are created in a software call [aseprite](https://www.aseprite.org/), the sources files for this project are available in the meta links at the top of this page.
