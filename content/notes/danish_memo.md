+++
title = "Danish memo"
date = "2022-01-05"
[taxonomies]
tags = ["danish", "learning", "language"]
[extra]
description = "danish language learning dump"
+++

{{ image(image_path="./images/dk.png", alt="glitched flag of denmark") }}

## Pronunciation

### Æ Ø Å

**Æ** is pronounced like [ai] (lait)

**Ø** is pronounced like [eu] (heureux)

**Å** is pronounced like [o] (bol)

### Soft D

When to use the soft d :

- A vowel followed by **-d** or **-dd**
- A word ends with **-et**

Exceptions :

- A single syllable containing a vowel followed by **-ds** : the d become silent
- A single syllable containing a vowel followed by **-dt** : the d become silent

[Here's a pretty good video](https://www.youtube.com/watch?v=njWi-MA39YE) to help with the pronunciation.

## Grammar

### Grammatical gender

**en** : common gender

**et** : neutral gender

| Group | Singular indefinite | Singular definite | Plural indefinite | Plural definite |
| ----- | ------------------- | ----------------- | ----------------- | --------------- |
| 1     | en bil              | bil**en**         | bil**er**         | bil**erne**     |
| 2     | en dame             | dame**n**         | dame**r**         | dame**rne**     |
| 3     | et hus              | hus**et**         | hus**e**          | hus**ene**      |
| 4     | en film             | film**en**        | film-             | film**ene**     |

### Possessive pronouns

| Person  | Singular common gender | Singular neutral gender | Plural                |
| ------- | ---------------------- | ----------------------- | --------------------- |
| jeg     | **min** bil            | **mit** hus             | **mine** biler/huse   |
| du      | **din** bil            | **dit** hus             | **dine** biler/huse   |
| han/hun | **sin** bil            | **sit** hus             | **sine** biler/huse   |
| han     | **hans** bil           | **hans** hus            | **hans** biler/huse   |
| hun     | **hendes** bil         | **hendes** hus          | **hendes** biler/huse |
| vi      | **vores** bil          | **vores** hus           | **vores** biler/huse  |
| i       | **jeres** bil          | **jeres** hus           | **jeres** biler/huse  |
| de      | **deres** bil          | **deres** hus           | **deres** biler/huse  |

When to use **hans** and **hendes** :

- The possessive pronoun is alos the subject of the sentence (or part of the subject) : `Hans børn kommer i morgen` - His children come tomorrow.
- When the subject is not the owner : `Lisbeth kører hendes bil` - Lisbeth drive her car (in this case the car of someone else).

### Attributive adjective

| Gender                | Attributive adjective | Example                     | Translation               |
| --------------------- | --------------------- | --------------------------- | ------------------------- |
| Singular common       | Base form             | vinen er **god**            | the wine is good          |
| Singular neutral      | Base form + **-t**    | teaterstykket er **godt**   | the theatre piece is good |
| Plural common/neutral | Base form + **-e**    | teaterstykkerne er **gode** | the wines are good        |

### Doubling last consonants

A name finishing by a brief vowel followed by a consonant will **double the consonant** in **singular definite** and **plural forms**.

| Group | Singular indefinite | Singular definite | Plural indefinite | Plural definite |
| ----- | ------------------- | ----------------- | ----------------- | --------------- |
| 1     | en søn              | sønnen            | sønner            | sønnerne        |
| 2     | en bus              | bussen            | busser            | busserne        |
| 3     | en nat              | natten            | nætter            | nætterne        |
| 4     | en kop              | koppen            | kopper            | kopperne        |

### Short answers

Used to avoid answering a question by Yes or No only.

If the question contains a **modal verb** (vil, skal, må, kan) or an **auxiliary verb** : the verb is reused in the answer.

Construction :

- **Ja + det + verb + subject**.
- **Nej + det + verb + subject + ikke**.

Example : `Har du en kæreste? Ja, det har jeg.` - Do you have a girlfriend? Yes, I do.

If the question doesn't contain such verb : the verb **gør** is used instead.

Construction :

- **Ja + det + gør + subject**.
- **Nej + det + gør + subject + ikke**.

Example : `Arbejder du i weekenden? Je det gør jeg.` - Are you working on the weekend? Yes, I do.

### Subject inversion

There are three scenarios for wich the subject is inverted :

1. When asking a question (see Conjugation section below).
2. When starting a sentence with a **complement** or **adverb** : `I weekenden arbejder jeg.` - On weekends I work.
3. When starting a sentence with a **subordinate proposal** : `Når jeg arbejder, drikker jeg meget kaffe.` - When I work, I drink a lot of coffee.

### Dynamic and static prepositions

- When there's an idea of movement the preposition to use is **til** : `Jeg skal til Århus på lørdag.` - I go to Århus saturday.
- When there's an idea of staticity the preposition to use is **i** : `Lisa er I Berlin` - Lisa is in Berlin.

## Vocabulary

### Short sentences

**Ja, det gør jeg** : Yes, I do

**Vi ses senere** : See you later

**Hvor skal du hen ?** : Where are you going ? **hen** is an adverb that indicates movement

**Hvor arbejder du henne ?** : Where are you working ? **henne** is an adverb that indicates immobility

**Hvornår kommer du hjem ?** : When are you coming home ? **hjem** is an adverb that indicates movement

**Er du hjemme ?** : Are you home? **hjemme** is an adverb that indicates immobility

**Om en halv time** : In hal an hour

**Nogle** : Some

**I weekenden** : The weekend

**Jeg er 28 år** : I'm 28

**Hvad et der galt** : What's wrong?

### Interrogative words

| Danish  | English   |
| ------- | --------- |
| Hvor  | Where    |
| Hvem | Who   |
| Hvad  | What |
| Hvornår | When  |
| Hvofor  | Why    |
| Hvordan  | How  |
| Hvilken  | Which    |
| Hvad for en | Which one   |
| Hvor mange| How many    |
| Hvor længe | How long    |
| Hvor gammel | How old    |
| Hvis | Whose    |
| Hvor lang tid | How much time    |


### Useful words

**Mange** : Many

**Meget** : Much

**Altid** : Always

**Normalt** : Usually 

**Tit** : Often 

**Nogle gang** : Sometimes 

**Ikke så tit** : Not so often

**Aldrig** : Never 


### Numbers

| Number | Danish  | Cardinal  |
| ------ | ------- | --------- |
| 1      | en /et  | første    |
| 2      | to      | anden     |
| 3      | tre     | tredje    |
| 4      | fire    | fjerde    |
| 5      | fem     | femte     |
| 6      | seks    | sjette    |
| 7      | syv     | syvende   |
| 8      | otte    | ottende   |
| 9      | ni      | niende    |
| 10     | ti      | tiende    |
| 11     | elleve  | ellevte   |
| 12     | tolv    | tolvte    |
| 13     | tretten | trettende |
| 14     | fjorten | fjortende |
| 15     | femten  | femtende  |
| 16     | seksten | sekstende |
| 17     | sytten  | syttende  |
| 18     | atten   | attende   |
| 19     | nitten  | nittende  |
| 20     | tyve    | tyvende   |

### Days

| Danish  | English   |
| ------- | --------- |
| Mandag  | Monday    |
| Tirsdag | Tuesday   |
| Onsdag  | Wednesday |
| Torsdag | Thursday  |
| Fredag  | Friday    |
| Lørdag  | Saturday  |
| Søndag  | Sunday    |

## Conjugation

### Personal pronouns

| Danish                | English       |
| --------------------- | ------------- |
| jeg                   | I             |
| du                    | you           |
| han / hun / den / det | he / she / it |
| vi                    | we            |
| i                     | you           |
| de                    | they          |

### Infinitive

Most of the time finishing with **-e** and preceded by **at**.

Example : `at komme`.

### Present

Most of the time finishing with **-r**.

Example : `jeg kommer`.

### Questions

To ask a question, invert subject and verb position in the sentence.

### Negation

To use the negation use **ikke** after the verb.

### Negative questions

Construction : **Verb + subject + ikke**.

### Modal verbs

**At skulle** (should) :

Can be used to express the future.

Construction : **Present form of at skulle + infinitive of main verb**.

Example : `Jeg skal arbejde i weekenden` - I will work this weekend.

**At kunne** (could/know) :

Is used to express possibility and knowledge.

Example for possibility : `Jeg kan ikke komme i dag` - I can't come today.

Example for knowledge : `Jeg kan tale dansk` - I can talk danish.

**At måtte** (to be allowed to) :

Is used for :

- Ask for a permission.

Construction : **Må + subject + infinitive of the verb + object**

Example : `Må jegryge en cigaret ?` - Can i smoke a cigarette ?

- Express a logical obligation.

Example : `Det regner så jeg må tage bilen.` - It's raining therefor I need to take the car.

**At burde** (should) :

Is used to give advices.

Construction : **Present form of at burde + infinitive of main verb**

Example : `Du bør arbejde mindre.` - You should work less.
