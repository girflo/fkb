+++
title = "Glitched factory log"
date = "2022-07-26"
[taxonomies]
tags = ["ruby", "code", "rust"]
[extra]
links = [["homepage", "https://glitchedfactory.com/"]]
description = "dev log for the glitched factory"
+++

## Backstory

In 2019, after stumbling upon [an article about PNG glitch](http://ucnv.github.io/pnglitch/) and became obsessed with the [library](https://github.com/ucnv/pnglitch) created by the writer of the article. Beside the beautiful aesthetic of glitch art there's something interesting about the idea of creating a pleasing image out of something that is usually considered broken.

After a while I casually talked about this with a friend only for him to tell me that he also played with glitch art using After Effect. This conversation started a collaboration that we decided to call Glitched Factory.

## Glitched Factory output

You can find the result of this collaboration on [instagram](https://www.instagram.com/glitched_factory/) or [mastodon](https://mastodon.social/@GlitchedFactory) depending if you fancy a close or open garden.

An important achievement of the collaboration is the clip [Redsun by CONCEPTUAL](https://www.youtube.com/watch?v=79T8c0vW0ik) created only using glitched images created with our tool.

The tools used are mostly After Effect, Blender and a custom GUI software.

## GUI creation

When playing with the library I mostly created small wrapper around it that could be called in the console **[1]**. The development time was very short and worked perfectly for myself but my friend never used the console, didn't had the time or enough reason to start using it.

Therefore the first step of this collaboration was to create a GUI around pnglitch library.

### Let's start with Ruby

[Pnglitch](https://github.com/ucnv/pnglitch) is written in Ruby and it's also the language I'm working with daily so for the creation of this tool the language choice was easy.

At that point the only GUI library capable of creating binaries was Shoes version 2 **[2]**.

As the execution of the glitch algorithm are rather slow I implemented a small Ruby library to create and manage a small queue in a separate asynchronously to prevent the GUI to freeze. This library is called [Tiny conveyor](@/projects/tiny_conveyor.md).

After creating a working prototype that could run on Linux, MacOs and Windows we decided to create our own filters and add them to the GUI.

That's where the limits of Ruby became a little bit too obvious. Those new filters are manipulating all the pixels of the input image, depending on the image that can be a lot of pixel to handle.

Ruby is a great language but not the fastest one, so those filter needed to be written in a more efficiant language.

### Ruby yes but Rust as well

Using FFI it's pretty easy to call C libraries from Ruby code, but even better : Rust can be compiled into C libraries. As I wanted to try Rust for quite some time I picked this lanugage and start to rewrite the new filters in this language.

The Rust library is called [Arko](@/projects/png-glitch-art-tools/arko.md) and the Ruby library including the compiled C library for Linux, MacOs and windows as well as the ffi bindings is called [Rusty engine](@/projects/png-glitch-art-tools/rusty_engine.md).

Here a schema of the stack :

{{ image(image_path="./images/glitch_art_tools.svg", alt="Glitched factory diagram") }}

The code of the software is available on the [(legacy) Glitched factory page](@/projects/png-glitch-art-tools/legacy_glitched_factory.md).

### It's never really done is it ?

At this point I'm happy with the tool, it's not the sofware and the binaries are heavy **[3]** but all the needed features are implemented and a binary is available for Linux, MacOs and Windows. That's enough for me.

Unfortunately the main maintainer of Shoes 2 decided to stop working on the library and declared it abandonware. The app was already not super stable and from time to time crashed for no reasons so I knew that the current state will not allow this app to work well in the future.

As an alternative to Shoes 2 I found Glimmer, a Jruby GUI framework. After adapting the code from the previous app the result was promising but I learned a bit late that JRuby doesn't support FFI, which meant going back to the slow custom filters.

On top of that, setting up the development environment was combersome as Glimmer only worked with specific versions of JVM and JRuby.

The code for the prototype is available here : [Glitched Factory Jruby](https://gitlab.com/girflo/glitched-factory-jruby).

### Ok then, Rust all the way

During all this time I was using the wrong tool for the task to do, again Ruby is a great language but because it isn't compiled trying to create a desktop app with it only lead to useless complexity and slow output.

JRuby was a bit better regarding the compiled part but still far from being perfect.

The obvious choice of language for creating the software I wanted all this time was now Rust, it's fast, compile easily to the targeted OS and it's fun to work with.

But that change of language meant two things :

1. A GUI framework for Rust was necessary

2. Pnglitch needs to be also rewritten in Rust

For the first point egui was quickly adopted, even if the look doesn't blend in each OS it's fun and fast to work with.

The second point is an ongoing process that you can follow here : [Pnglitcher](@/projects/png-glitch-art-tools/pnglitcher.md).

**Notes**

**[1]** More info about this CLI tool here : [PNG glitcher](@/projects/png-glitch-art-tools/png_glitcher.md).

**[2]** The version 3 of Shoes is fully rewritten in JRuby and was still heavily WIP at this time.

**[3]** When creating the binaries Shoes include a entire Ruby runtime this leads to heavy binaries by default.
