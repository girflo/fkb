+++
title = "Ruby http requests"
date = "2021-11-26"
[taxonomies]
tags = ["ruby", "http", "code"]
[extra]
description = "memo for simple http request in Ruby"
+++

{{ small_image(image_path="./images/ruby.png", alt="glitched logo of ruby") }}

## GET query with ssl

```ruby
require 'net/http'

def perform_query(target_url)
  uri = URI.parse(target_url)
  http = Net::HTTP.new(uri.host, uri.port)
  http.use_ssl = true
  request = Net::HTTP::Get.new(uri.request_uri)
  http.request(request)
end
```

## GET query with headers

```ruby
require 'net/http'

def perform_query(target_url)
  uri = URI.parse(target_url)
  http = Net::HTTP.new(uri.host, uri.port)
  request = Net::HTTP::Get.new(uri.request_uri)
  request['header-key'] = 'header-value'
  http.request(request)
end
```
