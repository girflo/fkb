+++
title = "Logseq time tracking"
date = "2024-10-01"
[taxonomies]
tags = ["logseq"]
[extra]
description = "A naive way to track time in Logseq"
+++

## Setup

### Template creation

I like to organize my templates in a dedicated page so the first step is to create a template page. Inside we can add the following two templates:

**Time tracking entry**

```
- Time tracking entry
  template:: time tracking entry
  template-including-parent:: false
	- date:: <%today%>
	  hours:: 1
	  type:: timetracking
	  project::
```

**Time tracking overview**

```
- Time tracking overview
  template:: time tracking overview
  template-including-parent:: false
  collapsed:: true
	- query-table:: true
	  query-sort-by:: block
	  query-sort-desc:: true
	  query-properties:: [:block :page]
	  #+BEGIN_QUERY
	  {:title [:h2 "Time tracking"]
	   :query [:find (pull ?b [*])
	         :in $ ?current-page
	         :where
	         (property ?b :type "timetracking")
	         [?p :block/name ?current-page]
	         [?b :block/refs ?p]]
	   :inputs [:current-page]}
	  #+END_QUERY
```

Once added to the page I add them as Template (right click on the block bullet then `Make a Template`).

### Creation of the project page

This is just an empty page I'm creating for the project needing time tracking.

### Create time_tracking page

Again for now an empty page, this will later contains the log of hours tracked.

## How to use it

### Log hours

Now that everything is setup I simply add now entry in the time_tracking page using the `/template` command and use the Time tracking entry template. Once added I adjust date and hours as needed and take make project I want to tracking time for.

### Overview of logged hours

In the project page I then using the Time tracking overview template again using the `/template` command.

Here I don't have to change the content of the block but I like to display the list as table buy clicking on the table icon right of the block title and hiding the block and page attribute (the visibility toggles can be access by clicking the cogwheel icon right of the block title).
