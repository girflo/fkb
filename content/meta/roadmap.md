+++
title = "Roadmap"
weight = 1
[taxonomies]
tags = ["meta"]
[extra]
description = "where I keep track of what near future tasks"
+++

This is a summary of my priorities and focus.

## To do

The following projects are in dire need of speed improvement:
- [Pnglitcher](@/projects/png-glitch-art-tools/pnglitcher.md)
- [Arko](@/projects/png-glitch-art-tools/arko.md)
- [Dithor](@/projects/dithor.md)

## Done

- Extract the logic from rusty engine into a Rust crate : [Arko](@/projects/png-glitch-art-tools/arko.md)
- Use [Arko](@/projects/png-glitch-art-tools/arko.md) inside [Rusty engine](@/projects/png-glitch-art-tools/rusty_engine.md)
- Add support for multiple tags in Yoctolio
- Add support for a small description of the video in Yoctolio
- Rewrite the glitched Factory in JRuby with glimmer : [Glitched Factory Jruby](https://gitlab.com/png-glitch/glitched-factory-jruby).
- Rewite the glitched Factory in Rust : [Glitched factory](@/projects/png-glitch-art-tools/glitched_factory.md)
- Create binaries for glitched factory
- Write the content of the [Glitched factory](@/projects/png-glitch-art-tools/glitched_factory.md) page on the website
- Various improvements for glitched factory, at the moment it's working but lack of a good ui and a bit rough on the edges
- Add [Dithor](@/projects/dithor.md) to [Glitched factory](@/projects/png-glitch-art-tools/glitched_factory.md)
