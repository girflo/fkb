+++
title = "About"
weight = 1
[taxonomies]
tags = ["meta"]
[extra]
description = "a brief introduction and of myself and the of this website"
+++

{{ image(image_path="./images/me.jpg", alt="Glitched portrait") }}

I'm Flo (he/him), welcome to my website.

I created this piece of internet to help me keep track of my projects, my thoughts about them and their current states. Beside those projects I create notes primarily for myself and this website is closer to a wiki than a blog as I often revisit existing notes to change it.

There is no comment system on this site by design, if you wish to talk about something you read here I will be more than happy to respond to you on Mastodon.

As you can see by the amount of projects in this area, I love and work in the field of computer science and more specifically web development. I would like eventually to diversify the domain covered by those projects.

Beside computer related ones here are my current nerding topics :

- Comic books and graphic novels, especially french/belgian and japanese.
- Techno music, mainly listening but I'm slowly trying to produce.
- Mushrooms, I grew some not so long ago but it's not possible anymore at the moment. I will start again as soon as I can.
- Fermentation, I love the taste of fermented food, even though I'm really a novice here I would love to learn more about it.
- Glitch art, you can read more about it [here](@/projects/png_glitch_art_tools.md).
- Photography.
- Cinema.

I sometime (rarely) toot on [mastodon](https://mastodon.social/@radoteur), this is also the best way to contact me (🇬🇧/🇫🇷 fully supported, 🇩🇰/🇩🇪 in progress).

For most project listed on this website the should be a LICENSE file contained in their codebases.

The assets and texts present on the website are under [creative common](http://creativecommons.org/licenses/by/4.0/).

The logo was created using [dotgrid](https://100r.co/site/dotgrid.html) and modified with Dithor.

The website was created using [Zola](https://www.getzola.org) and the sources are available on [Gitlab](https://gitlab.com/girflo/fkb/-/tree/main).
