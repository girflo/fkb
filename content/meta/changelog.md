+++
title = "Changelog"
weight = 1
[taxonomies]
tags = ["meta"]
[extra]
description = "the timeline of the changes made on the website and on the projects listed here"
+++

A monthly log of the changes on the project and notes listed on the website.

## 2024

### November

- Creation of [Spectrogram](@/projects/spectrogram.md) page

### October

- Creation of [Logseq time tracking](@/notes/logseq_time_tracking.md) page
- Add low res option to [Dithor](@/projects/dithor.md)

## 2023
### November

- Creation of [Instax mini evo](@/notes/instax_mini_evo.md) page

### September

- Version 0.2.0 of [Tiny conveyor](@/projects/tiny_conveyor.md) released

### April

- Creation of a [logo](https://gitlab.com/girflo/yoctolio/-/blob/main/logo/yoctolio-color.png) for [Yoctolio](@/projects/yoctolio.md)
- Version 0.3.7 of the [Glitched factory](@/projects/png-glitch-art-tools/glitched_factory.md) released
- Version 0.4.0 of [Yoctolio](@/projects/yoctolio.md) released
- Version 0.3.0 & 0.3.1 of [Pnglitcher](@/projects/png-glitch-art-tools/pnglitcher.md) released
- Archive [Piddly](@/archive/piddly.md)
- Addition of a new navigation menu on this website
- Version 0.2.1, 0.3.0 and 0.3.1 of [Dithor](@/projects/dithor.md) released

### March

- Version 0.2.0 of [Dithor](@/projects/dithor.md) released
- Version 0.3.5 & 0.3.6 of the [Glitched factory](@/projects/png-glitch-art-tools/glitched_factory.md) released
- Small update on the [Glitched factory](@/projects/png-glitch-art-tools/glitched_factory.md)'s logo

## 2022

### December

- Version 0.3.3 & 0.3.4 of the [Glitched factory](@/projects/png-glitch-art-tools/glitched_factory.md) released


### November

- Creation of the [Dithor](@/projects/dithor.md) project page
- Creation of the [Glitched factory](@/projects/png-glitch-art-tools/glitched_factory.md) page content
- Version 0.3.1 & 0.3.2 of the [Glitched factory](@/projects/png-glitch-art-tools/glitched_factory.md) released

### October

- Version 0.3.0 of the [Glitched factory](@/projects/png-glitch-art-tools/glitched_factory.md) released
- New website for the [Glitched factory](https://glitchedfactory.com/)
  The source is available [here](https://gitlab.com/girflo/glitched-factory-website)
  The source for the previous version is available [here](https://gitlab.com/png-glitch/glitched-factory-website-legacy)
- Creation of the [Berlin notes](@/notes/berlin.md)

### September

- Version 0.1.3 of [Pnglitcher](@/projects/png-glitch-art-tools/pnglitcher.md) released

### August

- Creation of the [Wikipedia articles](@/notes/wikipedia_articles.md) note
- Version 0.2.2 and 0.2.3 of [Arko](@/projects/png-glitch-art-tools/arko.md) released
- Version 0.1.2 of [Pnglitcher](@/projects/png-glitch-art-tools/pnglitcher.md) released

### July

- Creation of the [Glitched factory log](@/notes/glitched_factory_log.md)

### June

- Creation of the [Pnglitcher](@/projects/png-glitch-art-tools/pnglitcher.md) project page

### April

- Version 0.2.0 and 0.2.1 of [Arko](@/projects/png-glitch-art-tools/arko.md) released

### February

- Version 0.3.0 of [Yoctolio](@/projects/yoctolio.md) released
- Creation of the [Png glitch art tools](@/projects/png_glitch_art_tools.md) project page

### January

- Creation of the [Arko](@/projects/png-glitch-art-tools/arko.md) project
- Creation of the [Roadmap](@/meta/roadmap.md)
- Use new domain name
- Creation of the [Danish memo](@/notes/danish_memo.md)

## 2021

### December

- Creation of the [Cistercian](@/projects/cistercian.md) project
- Version 0.2.0 of [Yoctolio](@/projects/yoctolio.md) released
- Version 0.1.0 of [Tiny conveyor](@/projects/tiny_conveyor.md) released

### November

- Creation of this website and of the pages for all the projects/notes.
