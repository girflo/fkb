+++
title = "Thrived"
date = "2021-11-19"
[taxonomies]
tags = ["js", "jquery", "python"]
[extra]
archive = true
year = "2020"
links = [["project", "https://gitlab.com/girflo/thrived-anthea"]]
description = "website for image upload an cropping"
+++

{{ image(image_path="./images/thrived.png", alt="glitched picture of Thrived cover") }}

For the album cover of [their EP Anthea](https://thrived.bandcamp.com/album/thrived-anthea), the band Thrived decided to generate an image of a flower using an AI. To train this AI they asked their listeners to pick an image of a flower among a preselected set and to crop it as they wanted.

This project is the website that allows the selection and cropping of the images. It was quickly developed because of the scheduled release date so the technologies used are not top notch but something that allowed me quick prototyping (flask and jquery).
