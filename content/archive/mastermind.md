+++
title = "Mastermind"
date = "2021-11-14"
[taxonomies]
tags = ["ruby", "cli"]
[extra]
archive = true
year = "2017"
links = [["project", "https://gitlab.com/girflo/mastermind"], ["documentation", "https://gitlab.com/girflo/mastermind/-/blob/master/Doc/Mastermind%20in%20ruby%20-%20Report.pdf"]]
description = "mastermind game in terminal"
+++

This project was created during my studies, which means that I will not work more on this but I wish to keep it for future references if needed.

The project is a game of mastermind in console. The goal of this game is to discover a combination of several marble chosen by the script among the following colors :

- g : green
- r : red
- b : blue
- y : yellow
- w : white
- o : orange

There are three level of difficulty :

| Level  | Marble to discover |
| ------ | ------------------ |
| Easy   | 4                  |
| Medium | 5                  |
| Hard   | 6                  |

The user has 8 guesses to find the correct combination, each time the script will return one of those three symbols for each marble :

| Symbol | Meaning                                                       |
| ------ | ------------------------------------------------------------- |
| .      | The marble is present in the combination but in another place |
| 0      | The marble is at this place                                   |
| x      | The marble isn't in the secret combination                    |
