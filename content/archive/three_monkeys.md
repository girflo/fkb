+++
title = "Three monkeys"
date = "2021-11-20"
[taxonomies]
tags = ["js", "html", "3d"]
[extra]
archive = true
year = "2020"
links = [["project", "https://gitlab.com/girflo/three-monkeys"], ["demo", "https://girflo.gitlab.io/three-monkeys/"]]
description = "in browser .obj viewer and manipulation tool"
+++

{{ image(image_path="./images/monkeys.png", alt="glitched picture of Suzanne") }}

A small in browser .obj viewer and manipulation tool created with [ThreeJS](https://threejs.org/).

This project was created for a website of a design studio, originally the two faces were the ones of the members of this studio, but for this archived version of the project I decided to replace them by the good old Suzanne from [Blender](https://www.blender.org/).
