+++
title = "Datamosher"
date = "2021-11-18"
[taxonomies]
tags = ["ruby", "gui"]
[extra]
archive = true
year = "2019"
links = [["project", "https://gitlab.com/girflo/datamosher"]]
description = ".avi datamoshing software" 
+++

This project was an attempt to create a small gui around [aviglitch](https://github.com/ucnv/aviglitch) using Shoes.
Since Shoes is now an abandonware this project will not be continued.

Aviglitch is a great little library created by the same author as [pnglitch](https://github.com/ucnv/pnglitch) and allows a quick and easy way to create [datamoshed videos](https://en.wikipedia.org/wiki/Compression_artifact#Artistic_use) by destroying oon purpose the .avi format.

This is still an area that interests me and I will probably (one day...) create another version of this tool. Unfortunately this library is written in Ruby, at first it was great because it allowed me to quickly understand it and play with it but Ruby is not made for creating small and portable binaries. Therefore to create an interesting software that can be easily distributed this library should be rewritten in a compiled language.

At the moment I'm thinking about Rust to do this job but that can always change as long as I didn't start to work on it.
