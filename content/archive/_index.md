+++
title = "archive"
weight = 4
sort_by = "date"
template = "section.html"
page_template = "page.html"
[extra]
archive = true
+++

The projects listed here are old, some of them created during my studies other when scratching an itch.

Most of them are bad and forgettable, but that's the exact purpose of this page. It allows me to have a reminder of those projects and to revisite them later on.
