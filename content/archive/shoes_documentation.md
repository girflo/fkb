+++
title = "Shoes 3 documentation"
date = "2021-10-01"
[taxonomies]
tags = ["logo", "ruby"]
[extra]
archive = true
year = "2020"
links = [["project", "https://framagit.org/Radoteur/shoes-unofficial-documentation/-/tree/master/"], ["website", "https://radoteur.frama.io/shoes-unofficial-documentation/"]]
description = "small unoffical version of the documentation for this library"
+++

{{ image(image_path="./images/shoes_logo.svg", alt="Shoes unofficial logo") }}

Shoes is a GUI library for Ruby, I used it when creating the now [legacy glitched factory](@/projects/png-glitch-art-tools/legacy_glitched_factory.md). The documentation was embeded in the software needed to create the binaries but it wasn't practical to browse, first of all because the search field was useless but mostly because the software had to bad habit to randomly crash. This website was my attempt to migrate this documentation to the browser.

Shoes 3 has since been abandonned so this website has no real use.
