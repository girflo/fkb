+++
title = "Hangman"
date = "2021-11-15"
[taxonomies]
tags = ["haskell", "cli"]
[extra]
archive = true
year = "2017"
links = [["project", "https://gitlab.com/girflo/hangman"]]
description = "hangman game in terminal with customizable dictionary"
+++

A simple hangman game with a custom dictionary, the repository contains a binary for linux as well as the sources that can be compiled using Ghc (Haskell's default compiler).

This project was created during my studies, which means that I will not work more on this but I wish to keep it for future references if needed.

The Dictionary file contains all words that can be asked by the game. The content of this file can be edited in an editor but it's preferable to use the in-game function to add a word.

The goal of this game is to discover a word chosen by the script. The user needs to input a letter each turn as a guess. If the letter is contained in the secret word the script will show the word with the letter(s) visible for the user otherwise the user will lose one of its 8 lifes.

If the word is fully discovered the user wins the game but if the number of lives is 0 the user loses the game.
