+++
title = "Piddly"
date = "2021-11-21"
[taxonomies]
tags = ["css", "html"]
[extra]
archive = true
year = "2021"
links = [["project", "https://gitlab.com/girflo/piddly"]]
description = "minimal set of css rules"
+++

After realizing I always used the same set of CSS rules for a new project it's easier to just create some kind of skeleton app that can be reused.

## Inspiration

The rules from this css file are higly inspired (or stolen, as you want) from the following projects :

- [Skeleton](http://getskeleton.com/)
- [Normalize](https://necolas.github.io/normalize.css/)
- [Stack exchange's font rules](https://meta.stackexchange.com/questions/364048/we-are-switching-to-system-fonts-on-may-10-2021)
- [Emoji as favicon trick](https://endtimes.dev/til/emojis-as-favicons/)
