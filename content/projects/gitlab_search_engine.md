+++
title = "Gitlab search engine"
date = "2021-11-18"
[taxonomies]
tags = ["haskell", "thesis"]
[extra]
year = "2017"
links = [["project", "https://gitlab.com/girflo/gitlab_search_engine"]]
description = "search engine for open source projects hosted on gitlab"
+++

{{ image(image_path="./images/gitlab.png", alt="Glitched logo of gitlab") }}

This repository contains the work done for the dissertation module of my MSc in computer science at the Oxford Brookes University, including a search engine written in Haskell using the [Scotty web framework](https://hackage.haskell.org/package/scotty) and several documents created alongside the engine.

## Documentation

The documents are :

- [Dissertation proposal](https://gitlab.com/girflo/gitlab_search_engine/-/blob/master/doc/Dissertation%20Proposal.pdf) : Created after the development stage, describe the future project.
- [Interim Report](https://gitlab.com/girflo/gitlab_search_engine/-/blob/master/doc/Interim%20Report.pdf) : A mid-term document explaining the current state of the project.
- [Presentation](https://gitlab.com/girflo/gitlab_search_engine/-/blob/master/doc/Presentation.pdf) : A support for the oral presentation.
- [Short paper](https://gitlab.com/girflo/gitlab_search_engine/-/blob/master/doc/Short%20paper.pdf) : A small document that linger over the scientific side of the project.
- [Final report](https://gitlab.com/girflo/gitlab_search_engine/-/blob/master/doc/Final%20Report.pdf) : The larger document explaining the creation process of the entire project.

Each of these documents can be viewed in their raw formats (LaTex or Mardown).

## Abstract

The goal of this dissertation is to create a search engine capable of gathering information autonomously about open source projects hosted on several Gitlab servers, and retrieve this information to fit a search query performed by a user while being the most accurate and quick as possible.
A literature review is conducted about the Trie and B-tree data structures and their efficiency within the scope of search engines as well as a study of the current state of web Git servers.
Finally the main decisions taken during the development process will be explained.

## Search Engine

The search engine is composed by the following scripts :

- [CustomTypes.hs](https://gitlab.com/girflo/gitlab_search_engine/-/blob/master/src/CustomTypes.hs) : Containing each type declaration and functions related to them.
- [DbSql.hs](https://gitlab.com/girflo/gitlab_search_engine/-/blob/master/src/DbSql.hs) : Containing every CRUD function for the SQL database.
- [JSONParser.hs](https://gitlab.com/girflo/gitlab_search_engine/-/blob/master/src/JSONParser.hs) : Containing functions for JSON parsing (JSON files are created from Gitlab API response).
- [Main.hs](https://gitlab.com/girflo/gitlab_search_engine/-/blob/master/src/Main.hs) : Containing the Scotty routing and templates functions.
- [Query.hs](https://gitlab.com/girflo/gitlab_search_engine/-/blob/master/src/Query.hs) : Containing function for creating JSON files by querying Gitlab's API.
- [Research.hs](https://gitlab.com/girflo/gitlab_search_engine/-/blob/master/src/Research.hs) : Containing search functions used for the comparison between SQL and Trie performances.
- [Session.hs](https://gitlab.com/girflo/gitlab_search_engine/-/blob/master/src/Session.hs) : Containing functions creating and managing the Scotty's sessions.
- [TestDb.hs](https://gitlab.com/girflo/gitlab_search_engine/-/blob/master/src/TestDb.hs) : Containing benchmark creation functions.
- [Trie.hs](https://gitlab.com/girflo/gitlab_search_engine/-/blob/master/src/Trie.hs) : Containing every CRUD function for the Trie.
