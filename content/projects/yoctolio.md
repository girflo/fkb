+++
title = "Yoctolio"
date = "2021-11-24"
[taxonomies]
tags = ["rust"]
[extra]
year = "2021"
links = [["project", "https://gitlab.com/girflo/yoctolio"], ["demo", "https://www.gabrielli-av.net/"], ["rust crate", "https://crates.io/crates/yoctolio"]]
description = ".csv configured video portfolio generator"
+++

{{ image(image_path="./images/yoctolio.png", alt="glitched logo of Yoctolio") }}


## Backstory

A couple of months ago my friend had the need for a quick web page containing a selection of his work. As the requirements was pretty simple I decided to create a small prototype directly in good'ol HTML + CSS, as all the videos featured on the website are hosted on YouTube this took me a couple of minutes and everyone was happy.

Now of course my friend kept on working and therefore producing more videos, videos that he wanted to showcase. Adding a small html snippet every time was of course not a problem but there was always a delay and that when I thought

"Is there a way to over-engineer that process and to increase my Rust knowledge at the same time ?"

## Requirements

To fit this use case Yoctolio needs to have the following features :

- Display Youtube videos : ¯\\\_(ツ)\_/¯.
- Compartmentation of videos : by categories and years inside those categories.
- Tags for videos
- Editable only with csv files : as my friend will add his videos I don't want to use a markup language that he will need to learn just for this project.

## Constraints

Because this project is also about learning there are some self imposed constraints :

- Written in Rust : as said before I wanted to learn a bit more of this language and a small project like this one is perfect for that.
- Create a static site : there's no need for a CMS here, I want to keep the first approach using HTML and CSS.
- Create more than one page : it's always good to have a little about page in there.
- Having as few dependencies as possible : there's no point in trying to learn a technologie if it's to rely on an already made solution.

## Getting started

### Installation

**Using Cargo :** `cargo install yoctolio`

### Initialize project

Once yoctolio installed you can run `yoctolio init` to initialize a new project.

After configurating everything to your liking you can simple run `yoctolio build` to compile your website.

## Configuration

### Add video content

The first step needed to add content on your portifolio is to create a file at the following path :
`config/config.txt`

As well as the `config/tags.txt` file with the following content :

```csv
1, directing
2, editing
```

The words `directing` and `editing` can be changed and any tag can be added as long as their id (the number in front of the name) is uniq.

Inside this file you can now add one line per section on the index page of the portfolio :

`Name of the section, file_name_of_the_section.txt`

Yoctolio will now generate a section called `Name of the section`, the videos you wish to include in this section need to be listed in the file you defined. In this case `config/file_name_of_the_section.txt`

To add a video you need to follow this pattern :

`Name of the video, youtube_url, tag_number, year, description`

Where :

- `Name of the video` is self explenatory
- `youtube_url` is the full youtube url example : `https://www.youtube.com/watch?v=79T8c0vW0ik`
- `tag` the id of the tag of the video, to use multiple tags add `-` between two tags example `1-3`
- `year` the year of the production example : `2021`
- `description` a small explanation, the one is optional and can be left out

### Changing the template

The default templates are visible in [the project repository](https://gitlab.com/girflo/yoctolio/-/tree/main/src/templates), to create your own you can simply create a `templates` folder with an `index.html` inside and it will be used by default.

#### Adding a page

If you want another page you can create a folder at the following path `templates/pages` containing any html page that will be included in you portfolio.

#### Adding assets

Whenever you want to include css, js, images or any asset to you projects you can simply create a folder at the following path `templates/assets`, the content of this folder will be copied to `public/assets` during compilation.

#### Adding partials

A partial is a small html snippet that can be easilly reused in different pages and template, it can be useful for header, footer, navigation...

To add partials to your porfolio you need to create the folder `templates/partials`, then inside create any html file. To call a partial from a page of the index.html you just have to include the following snippet : `{{!partial_name}}`.

So if you create `templates/partials/footer.html` it is called by using `{{!footer}}` in any template.

### Live reload

When working on your templates you probably want to regenerate automatically the website as doing it manually can be quickly become annoying.

To do so I usually use [entr](http://eradman.com/entrproject/) like that :

`ls templates/* | entr yoctolio build`
