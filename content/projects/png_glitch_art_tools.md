+++
title = "Png glitch art tools"
date = "2022-01-29"
[taxonomies]
tags = ["rust", "glitch", "ruby"]
[extra]
glitch = true
year = "2020"
links = []
description = "collection of tools used for glitching .png images"
+++

A list of the tools created for making glitch art. If you are interested in the development process of those tools you can read the [Glitched factory log](@/notes/glitched_factory_log.md).

## Softwares

- [Glitched factory](@/projects/png-glitch-art-tools/glitched_factory.md): the easiest way to glitch png images, you just have to download the software for your OS (Linux, OSX or Windows) and use the gui to mess with your images.
- [(legacy) Glitched factory](@/projects/png-glitch-art-tools/legacy_glitched_factory.md): you probably don't want to use this software, use the one above instead. I keep it on this website for archival and reference purposes.

## Libraries

Libraries used by the two softwares above, you can use them from a Rust or Ruby code base.

### Rust

Those libraries may be used to create glitched images directly in Rust. 

- [Pnglitcher](@/projects/png-glitch-art-tools/pnglitcher.md): implement all the png glitch algorithms (it's messing on purpose with the PNG file format).
- [Arko](@/projects/png-glitch-art-tools/arko.md): implement some pixel manipulation algorithms (think pixel sorting). JPEG is supported by this library alongside PNG.

### Ruby

These libraries may be used to create glitched images directly in Ruby.

- [Rusty engine](@/projects/png-glitch-art-tools/rusty_engine.md): this is a wrapper for Arko, so it provides the exact same features but can be called from a Ruby codebase.
- [Shattered machine](@/projects/png-glitch-art-tools/shattered_machine.md): this library is bundle rusty engine and pnglitch, it was the main library used in the legacy version of the glitched factory but it's still working fine.


