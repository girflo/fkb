+++
title = "Tiny conveyor"
date = "2021-11-23"
[taxonomies]
tags = ["ruby", "asynchronous"]
[extra]
year = "2021"
links = [["project", "https://gitlab.com/girflo/tiny_conveyor"], ["ruby gem", "https://rubygems.org/gems/tiny_conveyor"]]
description = "asynchronous tasks queue in ruby"
+++

{{ image(image_path="./images/tiny_conveyor.png", alt="Glitched icon for tiny conveyor") }}

This gem is a tiny wrapper for [async](https://github.com/socketry/async) that can create and execute a queue of tasks, each of the task run in a separated thread.

## Setup

To use this gem you can simply install it with gem :

```ruby
gem install tiny_conveyor
```

or add the following line to your Gemfile :

```ruby
gem 'tiny_conveyor'
```

## Usage

### Instantiation

```ruby
require 'tiny_conveyor'
conveyor = TinyConveyor.new
```

### Run task

```ruby
action = proc { puts 'Run in a separated thread' }
conveyor.add_parcel('first action', 'a simple task that print a sentence', action)
conveyor.start_belt
```

### Remove unstarted task

```ruby
conveyor.remove_parcel_by_uuid('uuid_of_the_parcel')
```

### Check if a task is running

```ruby
conveyor.running?
```

### Check current pending tasks

```ruby
conveyor.parcels
```

## Acknowledgements

Icons made by xnimrodx from [flaticon](https://www.flaticon.com/)
