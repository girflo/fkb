+++
title = "Spectrogram"
date = "2024-11-15"
[taxonomies]
tags = ["rust", "audio"]
[extra]
year = "2024"
links = [["project", "https://gitlab.com/girflo/spectogram"]]
description = "software to create spectrogram"
+++

{{ image(image_path="./images/spectrogram.png", alt="spectrogram ui") }}

## What's this project

This is a GUI tool to create a [spectrogram](https://en.wikipedia.org/wiki/Spectrogram) from a wav file. The core library is [sonogram](https://github.com/psiphi75/sonogram/). 
The GUI library is [iced](https://iced.rs/). 

You can choose to export the spectogram as png or csv, choose among multiple color theme.

Example of spectogram created by this program:

{{ image(image_path="./images/spectrogram_example.png", alt="spectrogram example") }}
