+++
title = "Dithor"
date = "2022-11-18"
[taxonomies]
tags = ["rust", "pixel", "dither"]
[extra]
year = "2022"
links = [["project", "https://gitlab.com/girflo/dithor"], ["rust crate", "https://crates.io/crates/dithor"]]
description = "library for pixelisate/dithering images"
+++

## What is this library

### Basic principles

This library rewrite an image given as input using only two colors a la [dithering](https://en.wikipedia.org/wiki/Dither).

The image is spit in many squares of 5x5 pixels for high resolution mode, or 15x15 pixels for low resolution mode. For each of those squares the average [relative luminance](https://en.wikipedia.org/wiki/Relative_luminance) is calculated.

This luminance is then used to assign a level to the square. The level is between 0 and 9, 0 for completely bright and 9 fully dark.

#### High resolution

The content of the square is then filled with the following pattern (depending on the level):

{{ image(image_path="./images/dithor_gradiant.png", alt="dithor gradiant for levels") }}

Here's a picture showing the same patterns on a bigger surface:

{{ image(image_path="./images/dithor_pattern.png", alt="dithor gradiant big surface") }}

Finally here's an example using the painting "The swan, no 1" from Hilma af Klint:

{{ image(image_path="./images/dithor_example.png", alt="dithor version of the swan") }}

#### Low resolution

The content of the square is then filled with the following pattern (depending on the level):

{{ image(image_path="./images/dithor_gradiant_low_res.png", alt="dithor gradiant for level") }}

And here's the same example in low res:

{{ image(image_path="./images/dithor_example_low_res.png", alt="dithor version of the swan in low res") }}

### Using colors

When the third parameter is true the output will be in color, the brightest and darkest colors from the 25 or 225 pixels are used to fill the patterns showed above.

Example in high res: 

{{ image(image_path="./images/dithor_example_colors.png", alt="dithor version of the swan in colors") }}

Example in low res: 

{{ image(image_path="./images/dithor_example_low_res_colors.png", alt="dithor version of the swan in colors and low res") }}

## How to use it

[Crate.io](https://crates.io/crates/dithor)

To use this library you can simply call it using:

```rust
dithor::dithor("input.jpg", "output.jpg", true, true, true);
```

**Params**

- input: path to the input image, can be a jpg or png
- output: path to the output image, can be a jpg or png. If the file already exists it will be overwritten
- overwrite: specify if the output file should overwrite any existing image with the same name
- color: boolean, if true the output image will be in color otherwhise it will be in black and white
- high_res: boolean, if true will dither 5x5 pixels if false dither 15x15 pixels
