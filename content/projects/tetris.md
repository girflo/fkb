+++
title = "Tetris"
date = "2021-11-16"
[taxonomies]
tags = ["haskell", "java", "gui"]
[extra]
year = "2017"
links = [["project", "https://gitlab.com/girflo/tetris"], ["functional part", "https://gitlab.com/girflo/tetris/-/tree/master/Functional"], ["procedural part", "https://gitlab.com/girflo/tetris/-/tree/master/Procedural"]]
description = "two versions of the game tetris in two different programming paradigms"
+++

{{ image(image_path="./images/tetris.png", alt="glitched picture of Tetris") }}

The goal of this project is to create two versions of the game Tetris in two different paradigms:
imperative and functional. The two programming languages chosen for this task are respectively Java and Haskell.

This project was created during my studies, which means that I will not work more on this but I wish to keep it for future references if needed.

A document containing a complete explanation of the choices made for this project as well as a detailed comparison of the two programs using a compiler can be found in the [Doc/ diretory](https://gitlab.com/girflo/tetris/-/blob/master/Doc/Tetris.pdf).

## What is Tetris

For the few that don't know this game, Tetris is a cult puzzle type video game created by Alexey Pajitnov in 1984. The goal of the game is to create horizontal lines organizing small pieces together. Those pieces are called tetrominos.

The available tetrominos will be called by the following names in this document:

- i-tetromino
- o-tetromino
- t-tetromino
- j-tetromino
- l-tetromino
- z-tetromino
- s-tetromino

Each tetromino is a layout of four blocks.
The game will create one of those tetrominos and make it go down a 20/10 matrix, during the fall the user can change the tetromino’s orientation and it’s position but once it reach the bottom of the matrix or another tetromino it will be fixed.
Finally if one horizontal line is full of blocks it will disappear and give the user some points.

## Features

### Tetrominos

The program should be able to randomly choose between one of the seven tetrominos. The current tetromino should move one square to the right or to the left when the user presses a specific key.

### Blocks

Each tetromino is composed of four blocks, those blocks need to act independently. The main reason is that when a line is completed we don’t want the entire tetromino to disappear but only the block contains on the line. That’s why when a tetromino is fixed we need the program to take only into account blocks.

Nonetheless when the tetromino is still moving the block should behave as a whole.

### Matrix

The program needs to create the matrix of the game of the user; this matrix will have twenty horizontal lines and ten vertical ones.

The matrix needs to show the fixed tetrominos, the current one which can be laterally moved and which direction can be switched by the user.

When the user fills an entire horizontal line it should disappear and when a vertical one is filled the
game must stop.

### Drop

Every x seconds the current falling piece should drop 1 cell vertically. The value of x is defined by the current level.

### Level

Each time the user reachs 10 000 points the game changes the level which means that the falling speed of the current tetromino will be increased.

### Score

Following the original Nintendo scoring system here are the scores :

| Level | 1 line        | 2 lines        | 3 lines        | 4 lines         |
| ----- | ------------- | -------------- | -------------- | --------------- |
| 1     | 40            | 100            | 300            | 1200            |
| n     | 40 \* (n + 1) | 100 \* (n + 1) | 300 \* (n + 1) | 1200 \* (n + 1) |

Each 1000 point the game level is upgraded.

### Gravity

When a line is destroyed, each line above goes down one line, yet the gravity is a little bit specific, which means that even when a block is above a hole it won’t go down further than one line.

### Interface

The interface should contain on the left the matrix and a side panel on the right showing the score and the next tetromino.
