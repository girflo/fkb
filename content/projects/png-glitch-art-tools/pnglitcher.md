+++
title = "Pnglitcher"
date = "2022-06-08"
[taxonomies]
tags = ["rust", "glitch"]
[extra]
glitch = true
year = "2022"
links = [["project", "https://gitlab.com/girflo/PNGlitcher"], ["rust crate", "https://crates.io/crates/pnglitcher"]]
description = "library allowing you to glitch PNG images"
+++

This crate contains a library allowing you to glitch PNG images. Most of the code comes from the [PNG encoder](https://github.com/image-rs/image-png) written for the wonderful [Image crate](https://github.com/image-rs/image).
The idea here is to provide a tool similar to the Ruby gem [PNGlitch](https://github.com/ucnv/pnglitch) for Rust. The output of an image will not be the exact same as PNGlitch, it's simply used for inspiration.

At the moment this cargo is in heavy WIP, any release could introduce breaking changes so please use this crate for having fun and create weird looking PNG images.

## How to use

### Duplicate

#### Single filter

For each occurence this algorithm will copy data from another part of the image, then force the png to use a given filter.

**Params**

- input(str): The path to the input png image.
- output(str): The path to the output png image (without extension).
- overwrite: specify if the output file should overwrite any existing image with the same name
- filter(u8): From 0 to 5, define the filter used when saving the image.
- occurence(usize): Occurence of the glitch, 1 means every column of the image.
- gap(usize): The number of column skipped before the first occurence

**Examples**

```rust
pnglitcher::duplicate("input1.png", "output1", true, 3,  1, 0);
pnglitcher::duplicate("input2.png", "output2", true, 0, 100, 1000);

```

#### Sample

Create a sample using duplicate algorithm for all filters for a given image

**Params**

- input(str): The path to the input png image.
- output(str): The path to the output png image (without extension).
- overwrite: specify if the output file should overwrite any existing image with the same name
- occurence(usize): Occurence of the glitch, 1 means every column of the image.
- gap(usize): The number of column skipped before the first occurence

**Examples**

```rust
pnglitcher::duplicate_sample("input1.png", "output1", true, 1, 0);
pnglitcher::duplicate_sample("input2.png", "output2", true, 100, 1000);
```

### Replace

#### Single filter

For each occurence this algorithm will replace the data with 0, then force the png to use a given filter.

**Params**

- input(str): The path to the input png image.
- output(str): The path to the output png image (without extension).
- overwrite: specify if the output file should overwrite any existing image with the same name
- filter(u8): From 0 to 5, define the filter used when saving the image.
- occurence(usize): Occurence of the glitch, 1 means every column of the image.

**Examples**

```rust
pnglitcher::replace("input1.png", "output1", true, 0, 1);
pnglitcher::replace("input2.png", "output2", true, 4, 1);

```

#### Sample

Create a sample using duplicate algorithm for all filters for a given image

**Params**

- input(str): The path to the input png image.
- output(str): The path to the output png image (without extension).
- overwrite: specify if the output file should overwrite any existing image with the same name
- occurence(usize): Occurence of the glitch, 1 means every column of the image.

**Examples**

```rust
pnglitcher::replace_sample("input1.png", "output1", true, 1);
pnglitcher::replace_sample("input2.png", "output2", true, 10);
```

### Variable filter

Save each lines with a different png filter.

**Params**

- input(str): The path to the input png image.
- output(str): The path to the output png image (without extension).
- overwrite: specify if the output file should overwrite any existing image with the same name
- random(boolean): Defines if the choice of the filter is random or sequential.

**Examples**

```rust
pnglitcher::variable_filter("input1.png", "output1", true, true);
pnglitcher::variable_filter("input2.png", "output2", true, false);
```

### Wrong filter

#### Single filter

Force the png encoder to use a wrong filter

**Params**

- input(str): The path to the input png image.
- output(str): The path to the output png image (without extension).
- overwrite: specify if the output file should overwrite any existing image with the same name
- filter(u8): From 0 to 5, define the filter used when saving the image.

**Examples**

```rust
pnglitcher::wrong_filter("input1.png", "output1", true, 0);
pnglitcher::wrong_filter("input2.png", "output2", true, 2);

```

#### Sample

Create a sample using wrong filter algorithm for all filters for a given image**Params**

- input(str): The path to the input png image.
- output(str): The path to the output png image (without extension).
- overwrite: specify if the output file should overwrite any existing image with the same name

**Examples**

```rust
pnglitcher::wrong_filter_sample("input1.png", "output1", true);
pnglitcher::wrong_filter_sample("input2.png", "output2", true);

```
