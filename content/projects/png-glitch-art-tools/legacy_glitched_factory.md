+++
title = "(legacy) Glitched factory"
date = "2021-11-20"
[taxonomies]
tags = ["ruby", "gui", "glitch"]
[extra]
glitch = true
year = "2020"
links = [["project", "https://gitlab.com/girflo/legacy-glitched-factory"], ["homepage", "https://glitchedfactory.com/"]]
description = "software for glitching .png images"
+++

{{ image(image_path="./images/legacy_glitched_factory.png", alt="legacy glitch factory logo") }}

## About

This project contains the code for compiling the GlitchedFactory as well as the current version of the binaries (windows, linux and osx). To get an older version you can find them in the git repo.

This project is created using Shoes.
Since Shoes is now an abandonware this project will not be continued but a new version is in progress of rewritting using Glimmer.

## Specs

### Initialization

You need Rspec to run the specs for the project, to install it run `bundle install` in the root folder.

### Running

To run the specs simply run the following command `bundle exec rspec`

## Compile the project

### Linux

#### .deb package

For creating a .deb package you need to copy `script/utils/merge-lin.rb` inside `~/.shoes/package/` on the linux setup then run the following commands from the root folder of glitched factory:

```sh
scp script/utils/linux_packager.sh user@host:linux_packager.sh
ssh user@host:
./linux_packager.sh
```

### OSX

```sh
cd script/
mkdir dmg
cd dmg
ruby ../utils/dmg_packager_osx.rb ../utils/dmg_info_osx.yml
```

### Window

Copy `script/utils/compile_glitched_factory.bat` to the vm and run it as administrator

## Create new release

```sh
cd script
./create_new_version.sh <version number>
```
