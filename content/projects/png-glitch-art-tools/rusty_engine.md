+++
title = "Rusty engine"
date = "2021-11-22"
[taxonomies]
tags = ["rust", "ruby", "glitch"]
[extra]
glitch = true
year = "2021"
links = [["project", "https://gitlab.com/girflo/rusty-engine"], ["ruby gem", "https://rubygems.org/gems/rusty_engine_ffi"]]
description = "tiny image manipulation library with four different effects"
+++

{{ image(image_path="./images/rusty_engine.png", alt="Glitched icon for rusty engine") }}

This project is a tiny image manipulation library with four different functions and provides an efficient way to perform CPU intensive tasks.

It's written in Rust but is precompiled for Linux, macos and windows that can be called from a Ruby file through FFI.

## Ruby setup

The project is already precompiled in three different c libraries (stored in `lib/bin`) :

1. librusty_engine.dll (windows)
2. librusty_engine.dylib (macos)
3. librusty_engine.so (linux)

Therefore if you want to use it in a ruby script you can simply install the `rusty_engine` gem and simply `require 'rusty_engine_ffi'`.

## Usage

### Converter

Example :

```ruby
require 'rusty_engine_ffi'

RustyEngine.convert('images/input.jpg', 'images/output.jpg')
```

Arguments :

1. input file path
2. output path file

### Slim

Example :

```ruby
require 'rusty_engine_ffi'

RustyEngine.slim('images/input.jpg', 'images/output.jpg', '90', 'global', '3', 'red', '99')
```

Arguments :

1. input file path
2. output path file
3. probability
4. probability area
5. direction
6. colors
7. colors probabilities

### Brush

Example :

```ruby
require 'rusty_engine_ffi'

RustyEngine.brush('images/input.jpg', 'images/output.jpg', '90', '5', '10', '2')
```

Arguments :

1. input file path
2. output path file
3. probability
4. min
5. max
6. direction

### Pixel sorting

Example :

```ruby
require 'rusty_engine_ffi'

RustyEngine.sort('images/input.jpg', 'images/output.jpg', '1', 'true', '0', '0', '30', 'true', '60', '100', '0')
```

Arguments :

1. input file path
2. output path file
3. smart sorting
4. detection type
5. min
6. max
7. multiple range
8. min 2
9. max 2
10. sorting by

## Development

### Project architecture

- `src/` contains the Rust code
- `target/` contains the compiled rust code (this directory is not present in git)
- `lib/` contains the files included in the gem
- `lib/bin/` contains the compiled rust libraries for osx, linux and windows
- `lib/rusty_engine.rb` is the ruby file that uses FFI to call the libraries inside `lib/bin/`

### Compilation

To compile this project you need the following softwares installed on you machine :

- [Rust and Cargo](https://www.rust-lang.org/tools/install)
- [Docker](https://www.docker.com/)
- [Cross](https://github.com/rust-embedded/cross)

For compiling the rust code for your architecture :
`cargo build --release`

For cross compilation, use cross :

**Windows**
`cross build --release --target x86_64-pc-windows-gnu`

**Linux**
`cross build --release --target x86_64-unknown-linux-gnu`

The compiled library is now available in the `target/release/` directory.

## Icon

Icons made by [Freepik](http://www.freepik.com/) from [www.flaticon.com](https://www.flaticon.com/)
