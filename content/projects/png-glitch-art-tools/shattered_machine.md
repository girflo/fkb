+++
title = "Shattered machine"
date = "2021-11-23"
[taxonomies]
tags = ["ruby", "glitch"]
[extra]
glitch = true
year = "2021"
links = [["project", "https://gitlab.com/png-glitch/shattered-machine"], ["ruby gem", "https://rubygems.org/gems/shattered_machine"], ["documentation", "https://www.rubydoc.info/gems/shattered_machine"]]
description = "ruby wrapper for rusty engine and pnglitch"
+++

{{ image(image_path="./images/shattered_machine.png", alt="Glitched icon for shattered machine") }}

The Shattered Machine is a gem that aim to create glitched png images easily.

## Features

- Converter : Convert a single jpg images or a directory full of jpg image into png
- Seven algorithms : Seven different algorithm with their own settings giving you lots of freedom to mess with your images
- Sampler : Giving you a quick overview of all algorithms effect on your image
- Folder glitching : Glitch easily all png images contained in a folder

## Usage/Examples

### Glitcher

```ruby

require 'shattered_machine'

io = ShatteredMachine::Io.new('input_image.png', 'output_folder', 'output_filename')
ShatteredMachine::Glitcher.new('Slim', io).call

```

### Sampler

```ruby

require 'shattered_machine'

io = ShatteredMachine::Io.new('input_image.png', 'output_folder', 'output_filename')
ShatteredMachine::Sampler.new(io).call

```

### Converter

```ruby

require 'shattered_machine'

io = ShatteredMachine::Io.new('input_image.jpg', 'output_folder', 'output_filename')
ShatteredMachine::Converter.new(io).call

```

## Development

### Install locally

The Shattered Machine using two main libraries for glitching :

- [pnglitch](https://github.com/ucnv/pnglitch)
- [rusty engine](https://duckpuck.net/projects/png-glitch-art-tools/rusty-engine/)

The rusty engine is written in rust and a compiled version for Windows, Mac os and Linux is already included in the gem.
To install the needed dependencies you need to install the [Ruby language](https://www.ruby-lang.org/en/) and [Bundler](https://bundler.io/).

Then run `bundle install` to fetch the needed gems.

### Create new version

- Fill the CHANGELOG with the changes contained in the new version
- Ensure the specs are green
- Change the versions in `lib/shattered_machine.rb` and `shattered_machine.gemspec`
- Create git tag
- Compile the gem : `gem build shattered_machine.gemspec`
- Push the new version : `gem push shattered_machine-x.x.x.gem`

### Specs

This project uses [Rspec](https://rspec.info/) for writing specs, to run them simply run `bundle rspec`

## Acknowledgements

This gem is relying heavily on the [pnglitch gem](https://github.com/ucnv/pnglitch) and the [rusty engine gem](https://gitlab.com/png-glitch/rusty-engine).
The png image used in the specs has been created with [Ronin](https://100r.co/site/ronin.html) while the jpg one is painting from Karl Wiener.
Icons made by [Freepik](http://www.freepik.com/) from [www.flaticon.com](https://www.flaticon.com/)
