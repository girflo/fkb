+++
title = "Arko"
date = "2022-01-29"
[taxonomies]
tags = ["rust", "glitch"]
[extra]
glitch = true
year = "2022"
links = [["project", "https://gitlab.com/girflo/arko"], ["rust crate", "https://crates.io/crates/arko"]]
description = "rust library providing pixel manipulation algorithms"
+++

arko is a small library providing three pixel manipulation algorithms :

- Brush : Create some kind of brush effect
- Slim : Copy a pixel over a ramdom numbers of pixel on the same line or column
- Sort : A classic pixel sorting

The library also contains a minimal image convertion tool (supporting png and jpg).

## How to use it

All of the algorithms can be used in four directions :

- bottom to top
- left to right
- right to left
- top to bottom

For algorithms using colors here are the complete list of available colors : "white", "black", "grey", "red", "yellow", "green", "cyan", "blue", "magenta".

### Brush

**Params :**

- in_img(str) : path to the input image
- out_img(ste) : path to the output image
- force_ouput_overwrite(bool) : flag to allow overwriting of the output image if present
- proba(i32) : the probability for the algorithm to have effect on a pixel
- min(i32) : the minimum of pixel affected when the algorithm starts
- max(i32) : the maximum of pixel affected when the algorithm starts

**Examples :**

```rust
arko::brush_bottom_to_top("input.png", "btt.png",true, 100, 4, 20);
arko::brush_left_to_right("input.png", "ltr.png",true, 100, 4, 20);
arko::brush_right_to_left("input.png", "rtl.png",true, 100, 4, 20);
arko::brush_top_to_bottom("input.png", "ttb.png",true, 100, 4, 20);
```

### Slim

#### Probability global

When using these functions the probability is the same applyed to all colors.

**Params :**

- in_img(str) : path to the input image
- out_img(ste) : path to the output image
- force_ouput_overwrite(bool) : flag to allow overwriting of the output image if present
- proba(i32) : the probability for the algorithm to effect the next pixel when activated
- colors(Vec < Colors >) : the list of colors affect by the algorithm

**Examples :**

```rust
arko::slim_bottom_to_top_per_color("input.png", "btt.png",true, vec![(Colors::Red, 20), (Colors::Yellow, 34), (Colors::Green, 56), (Colors::Cyan, 45), (Colors::Blue, 64), (Colors::Magenta, 23)]);
arko::slim_left_to_right_per_color("input.png", "ltr.png",true, vec![(Colors::Red, 20), (Colors::Yellow, 34), (Colors::Green, 56)]);
arko::slim_right_to_left_per_color("input.png", "rtl.png",true, vec![(Colors::Green, 56), (Colors::Cyan, 45), (Colors::Blue, 64)]);
arko::slim_top_to_bottom_per_color("input.png", "ttb.png",true, vec![(Colors::Cyan, 45), (Colors::Blue, 64), (Colors::Magenta, 23)]);
```

#### Probability per colors

When using these functions the probability is provided per color.

**Params :**

- in_img(str) : path to the input image
- out_img(ste) : path to the output image
- force_ouput_overwrite(bool) : flag to allow overwriting of the output image if present
- colors_proba(Vec<(Colors, i32)>) : affected colors with their probability

**Examples :**

```rust
arko::slim_bottom_to_top_per_color("input.png", "btt.png",true, vec![(Colors::Red, 20), (Colors::Yellow, 34), (Colors::Green, 56), (Colors::Cyan, 45), (Colors::Blue, 64), (Colors::Magenta, 23)]);
arko::slim_left_to_right_per_color("input.png", "ltr.png",true, vec![(Colors::Red, 20), (Colors::Yellow, 34), (Colors::Green, 56)]);
arko::slim_right_to_left_per_color("input.png", "rtl.png",true, vec![(Colors::Green, 56), (Colors::Cyan, 45), (Colors::Blue, 64)]);
arko::slim_top_to_bottom_per_color("input.png", "ttb.png",true, vec![(Colors::Cyan, 45), (Colors::Blue, 64), (Colors::Magenta, 23)]);
```

### Sort (Brut)

These functions will affect the entire image and sort pixels either horizontally either vertically.

**Params :**

- in_img(str) : path to the input image
- out_img(ste) : path to the output image
- force_ouput_overwrite(bool) : flag to allow overwriting of the output image if present

**Examples :**

```rust
arko::sort_brut_bottom_to_top("input.png", "btt.png", true);
arko::sort_brut_left_to_right("input.png", "ltr.png", true);
arko::sort_brut_right_to_left("input.png", "rtl.png", true);
arko::sort_brut_top_to_bottom("input.png", "ttb.png", true);
```

### Sort (Smart)

**Params :**

- in_img(str) : path to the input image
- out_img(ste) : path to the output image
- force_ouput_overwrite(bool) : flag to allow overwriting of the output image if present
- detection_type(i32) :
  - 0 : detection by lightness
  - 1 : detection by color group
- detection_min(i32) : the minimum of pixel affected when the algorithm starts (first range)
- detection_max(i32) : the maximum of pixel affected when the algorithm starts (first range)
- multiple_range(bool) : either use one or two ranges
- detection_min_2(i32) : the minimum of pixel affected when the algorithm starts (second range)
- detection_max_2(i32) : the maximum of pixel affected when the algorithm starts (second range)
- sorting_by(i32) :
  - 0 : sorting by hue
  - 1 : sorting by saturation

**Examples :**

```rust
arko::sort_bottom_to_top("input.png", "btt.png", true, 0, 50, 360, false, 0, 0, 1);
arko::sort_left_to_right("input.png", "ltr.png", true, 0, 50, 360, false, 0, 0, 0);
arko::sort_right_to_left("input.png", "rtl.png", true, 0, 50, 60, true, 70, 80, 1);
arko::sort_top_to_bottom("input.png", "ttb.png", true, 0, 50, 60, true, 90, 100, 0);
```

### Converter

**Params :**

- in_img(str) : path to the input image
- out_img(ste) : path to the output image
- force_ouput_overwrite(bool) : flag to allow overwriting of the output image if present

**Examples :**

```rust
arko::convert("input.png", "output.jpg", true);
```
