+++
title = "Glitched factory"
date = "2022-07-26"
[taxonomies]
tags = ["rust", "gui", "glitch"]
[extra]
glitch = true
year = "2022"
links = []
description = "software for glitching .png images"
+++

{{ image(image_path="./images/glitched_factory.png", alt="Glitched logo of Glitched factory") }}

The Glitched Factory is my most meaningfull project to date. It is both a software and an art project (confusing, I know).


## Software

[homepage](https://glitchedfactory.com/software/) - [codebase](https://gitlab.com/png-glitch/glitched-factory) - [development log](@/notes/glitched_factory_log.md)

Available for Linux, Macos and Windows this software provide a simple interface to create glitched from .png images. Most of the images present on this website are processed using this software.

The Glitched Factory is free and open source, it was developped to be used regardless of technical skills and knowledge.

On the homepage you can find a complete documentation covering all the features, this documentation is also embeded in the help tab of the software. You will also find a download link for each OS.

If you wish to compile the project from source you can find the necessary information on the README file in the codebase.

{{ itch(url="https://duckpuck.itch.io/glitchedfactory") }}

## Art project

[homepage](https://glitchedfactory.com) - [mastodon](https://mastodon.social/web/@GlitchedFactory) - [instagram](https://www.instagram.com/glitched_factory/)

Using the software above as source of inspiration this art project search way of use it alongside other software (Blender, After Effect, Photoshop, etc.) to create images and videos.

You can find all the images and videos on the website, mastodon or instagram.
