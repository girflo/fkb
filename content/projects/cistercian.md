+++
title = "Cistercian"
date = "2021-12-10"
[taxonomies]
tags = ["ruby", "svg"]
[extra]
year = "2021"
links = [["project", "https://gitlab.com/girflo/cistercian"]]
description = ".svg generator of cistercian numerals"
+++

{{ image(image_path="./images/1993.png", alt="dithered image of 1993 cistercian numeral") }}

A couple of months ago I came across a post on Reddit talking about the [Cistercian numerals](https://en.wikipedia.org/wiki/Cistercian_numerals). I found the concept interesting and wanted to create a small script to generate all the numbers as a separated svg file.

The script generates the 9999 svg files usings [lines](https://developer.mozilla.org/en-US/docs/Web/SVG/Element/line) but there's also a version of the same numerals using [paths](https://developer.mozilla.org/en-US/docs/Web/SVG/Element/path).

I might use the second one to create a font at some point. To create them I used [oslllo-svg-fixer](https://github.com/oslllo/svg-fixer).

